import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  token: any;

  constructor( private http: HttpClient ) {
    this.token = this.getToken();
  }

  getToken( ): any  {
    const httpOptions = {
      headers : new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      )};
    const paramsBody = new HttpParams()
        .set('grant_type', 'client_credentials')
        .set('client_id', '11d1a98e4f8c477b8f98fb868851f4c8')
        .set('client_secret', '2f93c4daef0140a18830e317ff1681c0');

    return this.http.post('https://accounts.spotify.com/api/token', paramsBody, httpOptions)
    .subscribe( data => data );



  }

  getQuery( query: string ): Observable<object>{
    const URL = `https://api.spotify.com/v1/${ query }`;
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`
    });

    console.log(this.token);

    return this.http.get(URL, { headers });
  }

  getNewRelases(): Observable<object>  {

    return this.getQuery(`browse/new-releases?limit=20`)
      // tslint:disable-next-line: no-string-literal
      .pipe( map ( data =>  data['albums'].items ));
  }

  getArtists( param: string ): Observable<object> {

    return this.getQuery(`search?q= ${ param } &type=artist&limit=15`)
      // tslint:disable-next-line: no-string-literal
      .pipe( map ( data =>  data['artists'].items ));
  }

  getArtist( param: string ): Observable<object> {

    return this.getQuery(`artists/${ param }`);
      // tslint:disable-next-line: no-string-literal
      // .pipe( map ( data =>  data['artists'].items ));
  }

  getTopTracks( id: string ): Observable<object> {
    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
      // tslint:disable-next-line: no-string-literal
      .pipe( map ( data =>  data['tracks'] ));
  }
}
