import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage',
})
export class NoimagePipe implements PipeTransform {
  transform(images: unknown[]): string {
    if (!images) {
      return 'assets/img/noimage.png';
    }
    if ( images.length > 0){
      // tslint:disable-next-line: no-string-literal
      return images[0]['url'];
    }else{
      return 'assets/img/noimage.png';
    }

    return null;
  }
}
