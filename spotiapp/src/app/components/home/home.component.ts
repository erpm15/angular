import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [],
})
export class HomeComponent implements OnInit {
  newSongs: any[] = [];
  loading: boolean;
  error: boolean;
  errorMsg: string;

  constructor(private spotify: SpotifyService) {
    this.error = false;
    this.loading = true;
    this.spotify.getNewRelases().subscribe(
      (data: any) => {
        // console.log( data.albums.items );
        this.newSongs = data;
        this.loading = false;
      },
      (respError) => {
        this.error = true;
        this.errorMsg = respError.error.error.message;
        this.loading = false;
      }
    );
  }

  ngOnInit(): void {}
}
