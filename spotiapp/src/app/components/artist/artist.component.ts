import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: [
  ]
})
export class ArtistComponent {

  artist: any = {};
  topTracks: object;
  loadingArtist = true;

  constructor( private router: ActivatedRoute, private spotify: SpotifyService) {
    this.router.params.subscribe( params => {
      this.getArtist(params.id);
      this.getTopTracks( params.id);
    });
  }

  getArtist( id: string ): void {
    this.loadingArtist = true;
    this.spotify.getArtist(id).subscribe( reps => {
      this.artist = reps;
      this.loadingArtist = false;
    });
  }

  getTopTracks( id: string ): void {
    this.spotify.getTopTracks( id ).subscribe( topTracks => this.topTracks = topTracks  );
  }

}
