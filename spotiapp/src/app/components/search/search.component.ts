import { Component } from '@angular/core';
import { asapScheduler } from 'rxjs';
import { SafeMethodCall } from '@angular/compiler';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
  ]
})
export class SearchComponent  {
  artists: any[] = [];
  loading: boolean;

  constructor( private spotify: SpotifyService ) { }

  shearch( param: string ): void {
    this.loading = true;
    this.spotify.getArtists(param)
      .subscribe( (reps: any) => {
        // console.log( reps.artists.items );
        this.artists = reps;
        this.loading = false;
      });
  }


}
