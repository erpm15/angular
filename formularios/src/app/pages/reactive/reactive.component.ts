import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorsService } from '../../services/validators.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {
  
  forma!: FormGroup;

  constructor( private fb: FormBuilder,
               private validators: ValidatorsService ) {

    this.createForm();
    this.setForm();
    this.createListeners();

   }

  ngOnInit(): void {
  }

  validateField ( field: string ) {
    let valid = this.forma.get(field)?.invalid && this.forma.get(field)?.touched;    

    return valid;
  }

  validpass(): boolean{

    return this.forma.get('pass1')?.value === this.forma.get('pass2')?.value ? false : true;
  }

  getArray ( name: string ): FormArray{

    return this.forma.get(name) as FormArray;
  }

  createForm(){
    this.forma = this.fb.group({
      name        : ['', [ Validators.required, Validators.minLength(3), this.validators.noHolas ] ],
      lastName    : ['', [ Validators.required, Validators.minLength(3) ] ],
      userName    : ['', Validators.required , this.validators.validateUserName ],
      email       : ['', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')] ],
      pass1       : ['', [ Validators.required, Validators.minLength(3) ] ],
      pass2       : ['', [ Validators.required, Validators.minLength(3) ] ],
      address     : this.fb.group( {
        department: ['',[ Validators.required, Validators.minLength(3) ] ],
        city      : ['',[ Validators.required, Validators.minLength(3) ] ]
      }),
      hobbies     : this.fb.array([])
    }, {
      validators: [ this.validators.equalFields('pass1','pass2') ]
    });

  };

  createListeners(){
    this.forma.valueChanges.subscribe( value => console.log(value) );
    this.forma.statusChanges.subscribe( value => console.log(value) );

    this.forma.get('name')?.valueChanges.subscribe( value => console.log(value) );
  }

  addHobby( name: string ){

    this.getArray(name).push( this.fb.control( '', [Validators.required, Validators.minLength(5)] ) )
    
  }

  deleteHobby( name: string, i: number ){
    this.getArray(name).removeAt(i);
  }

  save(){
    
    if (this.forma.invalid) {

      this.markFileds( this.forma );

      return;
    }

    console.log(this.forma);
    
  }

  markFileds( form: FormGroup ){

    Object.values(form.controls).forEach( control => {
      if ( control.invalid ) {

        if ( control instanceof FormGroup ){
          this.markFileds(control)
        }

        control.markAsTouched();
      }
    });
  }

  setForm(){

    // se puede usar el metodo forma.set o el forma.reset la diferencia es que el set requiere el objeto completo, 
    // el reset deja vacios los campos que no se le pasen en el objeto
    this.forma.reset({
      name: '',
      lastName: '',
      email: '',
      address:{
        department: '',
        city: ''
      }
    });

    /*
    // Una manera de cargar data a un array dinamico es:

    ['comer', 'dormir'].forEach(valor => this.getArray('hobbies').push( this.fb.control( valor, Validators.required ) ) )
    */   

  }

}
