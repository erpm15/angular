import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CountryService } from '../../services/country.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  user = {
    name: 'Eriks',
    lastName:'Ricardo',
    email: 'erpm-15@hotmail.com',
    country: 'COL',
    gender: 'M'
  }

  countries: any[] = [];

  constructor( private countryService: CountryService ) { }

  ngOnInit(): void {
    this.countryService.getCountry().subscribe( countries => {
      this.countries = countries;
      this.countries.unshift( {
        name: '[Seleccione País]',
        code: ''
      } );
    });
  }

  save( forma: NgForm  ){

    if (forma.invalid) {
      Object.values(forma.controls).forEach( control => {
        if ( control.invalid ) {
          control.markAsTouched();
        }
      });

      return;
    }

    console.log( forma.value);
  }

}
