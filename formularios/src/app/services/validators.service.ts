import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

interface validateError { [s: string]: boolean }

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  constructor() { }

  validateUserName( control: FormControl ): Promise<validateError | null> | Observable<validateError | null>  {

    if (!control.value) {
      return Promise.resolve(null);
    }

    return new Promise( ( resolve, reject ) => {

      setTimeout(() => {

        if (control.value === 'erpm-15') {
          resolve({ exist: true })
        }else {
          resolve ( null )
        }
        
      }, 3500);

    })

  }

  equalFields(f1: string, f2: string){

    return ( formGroup: FormGroup) => {

      if( formGroup.controls[f1].value === formGroup.controls[f2].value ) {
        formGroup.controls[f2].setErrors(null)
      } else{
        formGroup.controls[f2].setErrors( { notEquials: true } )
      }

    }
  }

  // se validara que el valor no contenga la palabra "holas"
  noHolas( control: FormControl ): validateError | null {

    if ( control.value?.toLowerCase() === 'holas' ){
      return {
        noHolas: true
      }
    }

    return null;
  }

}
