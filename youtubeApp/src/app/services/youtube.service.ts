import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { YouTubeResponse, Item, Video } from '../models/youtube.models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class YoutubeService {

  private youtubeUrl = 'https://www.googleapis.com/youtube/v3';
  private apikey = 'AIzaSyDlsP_KZU_TknYng2TmQCztWjz2sHBhSU0';
  private playListId = 'UUuaPTYj15JSkETGnEseaFFg';
  private nextPageToken = '';

  constructor( private httpClient: HttpClient ) {}

  getVideos(): Observable<Video[]>  {

    const url = `${this.youtubeUrl}/playlistItems`;
    const params = new HttpParams()
      .set('part', 'snippet')
      .set('maxResults', '10')
      .set('playlistId', this.playListId)
      .set('key', this.apikey)
      .set('pageToken', this.nextPageToken);

    return this.httpClient.get<YouTubeResponse>( url, { params } )
                .pipe(
                  map(
                    resp => {
                      this.nextPageToken = resp.nextPageToken;
                      return resp.items.map( item => item.snippet );
                      }
                  )
                );
  }
}


