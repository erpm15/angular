import { Component, OnInit, AfterViewInit } from '@angular/core';

import { ChatService } from '../../services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styles: [
  ]
})
export class ChatComponent implements OnInit, AfterViewInit  {

  mensaje = '';
  elemento: any;

  ngOnInit(): void {
    this.elemento = document.getElementById('app-mensaje');
  }

  ngAfterViewInit(): void {
  }

  constructor( public cS: ChatService ) {
    this.cS.cargarMensajes()
    .subscribe( () =>  {

        this.elemento.scrollTop = this.elemento.scrollHeight;

    });
  }

  enviarMensaje(): void{

    if ( this.mensaje.length === 0 ){
      return;
    }

    this.cS.agregarMensaje( this.mensaje )
                    .then(
                      () => this.mensaje = ''
                    )
                    .catch( (err) => console.log(err)  );

  }

}
