import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserModel } from '../models/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'https://identitytoolkit.googleapis.com/v1/accounts:';
  private apiKey = 'AIzaSyD8vmuoGBiMqxgGCyFxiZt8z9viEYEa-p0';

  userToken: string;

  // Create new user
  //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  // LogIn
  // https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]


  constructor(private http: HttpClient) { 
    this.readToken();
   }

  logOut() {
    localStorage.removeItem('token');

  }

  logIn(user: UserModel) {
    const authData = {
      ...user,
      returnSecureToken: true
    }

    return this.http.post(
      `${this.url}signInWithPassword?key=${this.apiKey}`,
      authData
    ).pipe(
      map( resp => {
        this.saveToken( resp['idToken'] );
        return resp;
      })
    );

  }

  newUser(newUser: UserModel) {
    const authData = {
      ...newUser,
      returnSecureToken: true
    }

    return this.http.post(
      `${this.url}signUp?key=${this.apiKey}`,
      authData
    ).pipe(
      map( resp => {
        this.saveToken( resp['idToken'] );
        return resp;
      })
    );
  }

  private saveToken ( idToken: string ){
    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let today = new Date();
    today.setSeconds(3600);

    localStorage.setItem('expires', today.getTime().toString());

  }

  readToken(){
    if ( localStorage.getItem('token') ) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }

  isAutenticated(): boolean  {

    if (this.userToken.length < 2) {
      return false
    }

    const EXPIRES = Number(localStorage.getItem('expires'));
    const EXPIRESDATE = new Date();
    EXPIRESDATE.setTime(EXPIRES);

    if (EXPIRESDATE > new Date() ){
      return true
    } else{
      return false
    }
    
  }

}
