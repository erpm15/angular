import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { UserModel } from '../../models/user.model';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  user: UserModel;
  rememberUser = false;

  constructor( private auth: AuthService,
                private router: Router ) { }

  ngOnInit() {
    this.user = new UserModel();
  }

  onSubmit(form: NgForm) {
    if (form.invalid) {
      return false;
    }
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor',
      icon: 'info'
    });
    Swal.showLoading();

    this.auth.newUser( this.user ).subscribe( resp => {

      Swal.close();

      if ( this.rememberUser ) {
        localStorage.setItem( 'email', this.user.email );
      }

      this.router.navigateByUrl('/home');
      
    }, ( error ) => {

      Swal.fire({
        title: 'Error al auntenticar',
        text: error.error.error.message,
        icon: 'error'
      });

    });
  }

}
