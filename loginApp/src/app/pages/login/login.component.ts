import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserModel } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
//import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: UserModel = new UserModel();

  rememberUser = false;
  
  constructor(private auth: AuthService,
              private router: Router) { }

  ngOnInit() {

    if (localStorage.getItem('email')) {
      this.user.email = localStorage.getItem('email');
      this.rememberUser = true;
    }
  }

  login(form: NgForm) {

    if (form.invalid) {

      return false;
    }

    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor',
      icon: 'info'
    });

    Swal.showLoading();

    this.auth.logIn(this.user)
      .subscribe(resp => {    

        Swal.close();

        if ( this.rememberUser ) {
          localStorage.setItem( 'email', this.user.email );
        }

        this.router.navigateByUrl('/home');

      }, (error) => {
     
        Swal.fire({
          title: 'Error al auntenticar',
          text: error.error.error.message,
          icon: 'error'
        });

      });
  }

}
