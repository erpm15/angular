import { Component, OnInit } from '@angular/core';
import { Marker } from '../../classes/marker.class';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EditMapComponent } from './edit-map.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  markers: Marker[] = [];
  lat = 51.678418;
  lng = 7.809007;

  constructor( public snackBar: MatSnackBar, public dialog: MatDialog ) {

    if (localStorage.getItem('markers')){
      this.markers = JSON.parse(localStorage.getItem('markers'));
    }
   }

  ngOnInit(): void {
  }

  addMarker( event ): void {
    const coords: { lat: number, lng: number  } = event.coords;
    const newMarker = new Marker( coords.lat, coords.lng );
    this.markers.push(newMarker);
    this.saveMarkers();
    this.snackBar.open('Marker added', 'Close', { duration: 2000 });
  }

  saveMarkers(): void {
    localStorage.setItem( 'markers', JSON.stringify(this.markers) );
  }

  deleteMarker(i: number ): void {
    this.markers.splice(i, 1);
    this.saveMarkers();
    this.snackBar.open('Marker deleted', 'Close', { duration: 2000 });
  }

  editMarker( marker: Marker): void {

    const dialogRef = this.dialog.open( EditMapComponent , {
      width: '250px',
      data: {title: marker.title, desc: marker.desc}
    });

    dialogRef.afterClosed().subscribe( (result: Marker) => {
      console.log('The dialog was closed');

      if (!result){
        return;
      }else{
        marker.title = result.title;
        marker.desc = result.desc;

        this.saveMarkers();
        this.snackBar.open('Marker Updated', 'Close', { duration: 2000 });
      }

    });

  }

}
