import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Marker } from '../../classes/marker.class';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-map',
  templateUrl: './edit-map.component.html',
  styleUrls: ['./edit-map.component.css']
})
export class EditMapComponent implements OnInit {

  form: FormGroup;

  constructor(
    public fB: FormBuilder,
    public dialogRef: MatDialogRef<EditMapComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Marker) {

      console.log(data);
      this.form = fB.group({
        title : data.title,
        desc: data.desc
       });
    }

  ngOnInit(): void {
  }

  saveChanges(): void {

    this.dialogRef.close( this.form.value );

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
