import { stringify } from '@angular/compiler/src/util';
import { Directive, EventEmitter, ElementRef, HostListener, Input, Output } from '@angular/core';
import { FileItem } from '../models/file-item';

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {

  @Input() files: FileItem[] = [];
  @Output() mouseOver: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  @HostListener('dragover', ['$event'])
  public onDragEnter( event: any ): void {
    this.mouseOver.emit( true );
    this._preventDefault(event);
  }

  @HostListener('dragleave', ['$event'])
  public onDragExit( event: any ): void {
    this.mouseOver.emit( false );
  }

  @HostListener('drop', ['$event'])
  public onDrop( event: any ): void {

    const data = this._getData( event );

    if ( !data ){
      return;
    }

    this._getFiles( data.files );
    this._preventDefault(event);

    this.mouseOver.emit( false );

  }

  private _getFiles( fileList: FileList ): void {
    Object.keys(fileList).map( key =>
      {
        const currentFile = fileList[key];

        if (this._validFile( currentFile  )){
          this.files.push( new FileItem( currentFile ) );
        }
      }
    );
  }

  private _getData( event: any ): any {
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
  }

  private _validFile( file: File ): boolean {

    if ( !this._droppedFile( file.name ) && this._pictureType(file.type) ){
      return true;
    }else{
      return false;
    }

  }

  private _preventDefault( event: Event ): void {
    event.preventDefault();
    event.stopPropagation();
  }

  private _droppedFile( fileName: string ): boolean {

    for ( const file of this.files ) {

      if ( file.fileName === fileName ) {
        return true;
      }
    }

    return  false;
  }

  private _pictureType( fileType: string ): boolean {
    return ( fileType === '' || fileType === undefined ) ? false : fileType.startsWith('image');
  }

}
