import { Component, OnInit } from '@angular/core';
import { FileItem } from '../../models/file-item';
import { UpdaloadPicturesService } from '../../services/updaload-pictures.service';

@Component({
  selector: 'app-load',
  templateUrl: './load.component.html',
  styles: [
  ]
})
export class LoadComponent implements OnInit {

  overDrop = false;
  files: FileItem[] = [];

  constructor( public updaloadPicturesService: UpdaloadPicturesService ) { }

  ngOnInit(): void {
  }

  uploadPictures(): void {
    this.updaloadPicturesService.uploadPicturesFireBase( this.files );
  }

  clearFileList(): void {
    this.files = [];
  }

}
