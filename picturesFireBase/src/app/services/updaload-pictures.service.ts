import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import * as firebase from 'firebase';
import { FileItem } from '../models/file-item';

@Injectable({
  providedIn: 'root'
})
export class UpdaloadPicturesService {

  private PICTURES_FOLDER = 'img';

  constructor( private angularFireStore: AngularFirestore, private angularFireStorage: AngularFireStorage) { }

  uploadPicturesFireBase( pics: FileItem[] ): void {

    for (const item of pics) {
      item.uploading = true;
      if ( item.progress >= 100 ) {
        continue;
      }

      const file = item.file;
      const fileNameFireBase = `${ Date.now() }-${ item.fileName }`;
      const filePath = `${this.PICTURES_FOLDER}/${fileNameFireBase}`;
      const fileRef  = this.angularFireStorage.ref( filePath );
      const uploadTask  = this.angularFireStorage.upload(filePath, file);

      uploadTask.percentageChanges().subscribe( progress => item.progress = Number(progress));

      uploadTask.snapshotChanges().pipe(
        finalize( () => fileRef.getDownloadURL().subscribe( url => {

          item.url = url;
          item.uploading = false;
          this.savePicture( { name: fileNameFireBase, url } );

        })
        )).subscribe();
    }

  }

  private savePicture( pic: { name: string, url: string } ): void{

    this.angularFireStore.collection( `/${this.PICTURES_FOLDER}` ).add(pic);

  }
}
