// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB37Uvgt-bgmayG025IchE41QMhDDWEy-o',
    authDomain: 'picturesfirebase.firebaseapp.com',
    projectId: 'picturesfirebase',
    storageBucket: 'picturesfirebase.appspot.com',
    messagingSenderId: '196411412569',
    appId: '1:196411412569:web:6a2a50a1c1499e6fe9e148'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
