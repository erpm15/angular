import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { MoviesServiceService } from '../../services/movies-service.service';
import { Movie } from '../../interfaces/nowPlayingResponse';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [],
})
export class HomeComponent implements OnInit, OnDestroy {
  public movies: Movie[] = [];
  public moviesSlideShow: Movie[] = [];
  @HostListener('window:scroll', ['$event'])

  onScroll(): void {

    const pos = (document.documentElement.scrollTop || document.body.scrollTop ) + 950 ;
    const max = ( document.documentElement.scrollHeight || document.body.scrollHeight );

    if (pos > max){

      if ( this.moviesServiceService.loading ){

        return;
      }

      this.moviesServiceService.getNowPlaying().subscribe( movies => {

        this.movies.push(...movies);

      });
    }
  }

  constructor(private moviesServiceService: MoviesServiceService) {}

  ngOnInit(): void {
    this.moviesServiceService.getNowPlaying().subscribe((movies) => {
      this.movies = movies;
      this.moviesSlideShow = movies;
    });
  }

  ngOnDestroy(): void {
    this.moviesServiceService.resetNowPlayingPage();
  }

}
