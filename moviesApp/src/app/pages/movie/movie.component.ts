import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesServiceService } from '../../services/movies-service.service';
import { MovieDetailsResponse } from '../../interfaces/movieDetailsResponse';
import { Location } from '@angular/common';
import { Cast } from '../../interfaces/creditsResponse';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styles: [
  ]
})
export class MovieComponent implements OnInit {

  public moviDetail: MovieDetailsResponse;
  public cast: Cast[];

  constructor( private activatedRoute: ActivatedRoute,
               private moviesServiceService: MoviesServiceService,
               private location: Location,
               private router: Router ) { }

  ngOnInit(): void {

    const { id } = this.activatedRoute.snapshot.params;

    combineLatest([
      this.moviesServiceService.getMovieDetails(id),
      this.moviesServiceService.getMovieCredits(id)
     ]).subscribe( ( [ movieDetails, cast ] ) => {
      if ( !movieDetails ){
        this.router.navigateByUrl('/home');

        return;
      }
      this.moviDetail =  movieDetails;
      this.cast = cast.filter( actor => actor.profile_path !== null );

     });
  }

  back(): void {
  this.location.back();
  }

}
