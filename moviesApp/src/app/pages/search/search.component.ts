import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesServiceService } from '../../services/movies-service.service';
import { Movie } from '../../interfaces/nowPlayingResponse';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
  ]
})
export class SearchComponent implements OnInit {

  public movies: Movie[] = [];
  public inputText: string;

  constructor( private activatedRoute: ActivatedRoute, private moviesServiceService: MoviesServiceService ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.moviesServiceService.searchMovies( params.param ).subscribe( movies => {
        this.movies = movies;
        this.inputText = params.param;
       } );
    });
  }

}
