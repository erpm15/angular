import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Movie, NowPlayingResponse, } from '../interfaces/nowPlayingResponse';
import { MovieDetailsResponse } from '../interfaces/movieDetailsResponse';
import { CreditsResponse, Cast } from '../interfaces/creditsResponse';

@Injectable({
  providedIn: 'root'
})
export class MoviesServiceService {

  private baseUrl = 'https://api.themoviedb.org/3';
  private NowPlayingPage = 1;
  public loading = false;

  constructor( private http: HttpClient) { }

  get params(): any{
    return{
      api_key: '77e8e0f66671c26208ae84e5cce7358e',
      language: 'en-US',
      page: this.NowPlayingPage
    };
  }

  resetNowPlayingPage(): void {
    this.NowPlayingPage = 1;
  }

  getNowPlaying(): Observable<Movie[]> {

    if ( this.loading){
      return of([]);
    }

    this.loading = true;
    return this.http.get <NowPlayingResponse> (`${ this.baseUrl }/movie/now_playing`, {
      params: this.params
    }).pipe(
      map( (resp) => resp.results ),
      tap( ()  => {
        this.NowPlayingPage += 1;
        this.loading = false;
      })
    );
  }

  searchMovies( query: string ): Observable<Movie[]> {

    const params = {...this.params, page: '1', query};

    return this.http.get<NowPlayingResponse>(`${ this.baseUrl }/search/movie`, {
      params
    }).pipe( map(resp => resp.results) );

  }

  getMovieDetails( id: string): Observable<MovieDetailsResponse> {

    return this.http.get<MovieDetailsResponse>(`${this.baseUrl}/movie/${id}`, { params: this.params })
    .pipe(
      catchError( err => of(null) )
    );

  }

  getMovieCredits( id: string): Observable<Cast[]> {

    return this.http.get<CreditsResponse>(`${this.baseUrl}/movie/${id}/credits`, { params: this.params })
    .pipe(
      map( credits => credits.cast ),
      catchError( err => of([]) )
    );

  }

}
